var getUser = (id, callback) =>{    //pass id & callback as argument function to the getUser function
  var user = {
    id: id,
    name: 'Joe'
  };
  setTimeout(() =>{   //simulate a delay of 2secs before calling the getUser function
    callback(user)
  }, 2000)   //excute the callback within the getUser function and pass in user object cos callback must return something to make sense
}

getUser(001, (user) =>{   //calling the getUser funtion and pass in the parameters and supply user as the parameter of the callback function
  console.log(user);
})
