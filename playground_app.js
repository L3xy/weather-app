const request = require('request');

request({
url: "https://api.darksky.net/forecast/a5fde306927ea0323eea3038baf46321/6.6186624,3.3583598",
json: true
}, (err, res, body) => {
  if(!err && res.statusCode === 200){
      console.log(body.currently.temperature);
  } else{
    console.log("Unable to fetch weather");
  }
});
