const request = require('request');

let getWeather = (lat, lng, callback) => {
  request({
  url: `https://api.darksky.net/forecast/a5fde306927ea0323eea3038baf46321/${lat},${lng}`,
  json: true
  }, (err, res, body) => {
    if(!err && res.statusCode === 200){
        callback(undefined, {
          temp: body.currently.temperature,
          apparent_temp: body.currently.apparentTemperature
        })
    } else{
      callback("Unable to fetch weather");
    }
  });
}

module.exports.getWeather = getWeather;
